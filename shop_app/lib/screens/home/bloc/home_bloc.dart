import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:shop_app/data/models/product_resp.dart';
import 'package:shop_app/data/repositories/product_api.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc(this.productApi) : super(HomeInitial()) {
    on<LoadDataEvent>(_loadDataEvent);
  }

  final ProductApi productApi;

  Future<void> _loadDataEvent(
      LoadDataEvent event, Emitter<HomeState> emit) async {
    print("LOG_HomeBloc_loadDataEvent(): start");
    try {
      // change loading state
      emit(state.copyWith(isLoading: true));
      // call api
      final productResp = await productApi.getAll();
      emit(state.copyWith(listProduct: productResp, isLoading: false));
    } catch (e) {
      print("LOG_HomeBloc_loadDataEvent(): exception = " + e.toString());
      emit(state.copyWith(listProduct: [], isLoading: false));
    }
  }
}
