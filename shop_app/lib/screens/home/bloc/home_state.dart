part of 'home_bloc.dart';

class HomeState {
  final List<ProductResp> listProduct;
  final bool isLoading;

  HomeState({required this.listProduct, required this.isLoading});

  HomeState copyWith({List<ProductResp>? listProduct, bool? isLoading}) {
    return HomeState(
        listProduct: listProduct ?? this.listProduct,
        isLoading: isLoading ?? this.isLoading);
  }
}

final class HomeInitial extends HomeState {
  HomeInitial() : super(listProduct: [], isLoading: false);
}
